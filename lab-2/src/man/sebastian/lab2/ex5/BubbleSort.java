package man.sebastian.lab2.ex5;
import java.io.*;

public class BubbleSort {

    public static void main(String[] args) throws IOException {
        int[] v = new int[10];
        int i;
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        for (i = 0; i < 10; i++)
            v[i] = Integer.parseInt(stdin.readLine());
        int aux;
        boolean stop;
        do {
            stop = true;
            for (i = 0; i < 9; i++)
                if (v[i] > v[i + 1])
                {
                    aux = v[i + 1];
                    v[i + 1] = v[i];
                    v[i] = aux;
                    stop = false;
                }
        }
        while (!stop);
        for (i = 0; i < 10; i++)
            System.out.print(v[i] + " ");
    }

}