package man.sebastian.lab2.ex6;
import java.util.Scanner;

public class Recursiv {
    private static int factorial(int n){
        if (n == 0)
            return 1;
        else
            return(n * factorial(n-1));
    }
    public static void main(String[] args){
        int i,fact;
        Scanner  num = new Scanner(System.in);
        System.out.println("Which is the number?");
        int N = num.nextInt();
        fact = factorial(N);
        System.out.println("Factorial of "+N+" is: "+fact);
    }
}
