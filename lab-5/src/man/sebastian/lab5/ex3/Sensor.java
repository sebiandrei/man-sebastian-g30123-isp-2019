package man.sebastian.lab5.ex3;

import java.util.Scanner;

abstract class Sensor {
    private String location;

    abstract int readValue();

    String getLocation(){
        System.out.println("Location:");
        Scanner in=new Scanner(System.in);
        location=in.nextLine();
        return location;
    }
}