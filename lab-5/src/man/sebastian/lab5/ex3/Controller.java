package man.sebastian.lab5.ex3;

class Controller {
    private TemperatureSensor ts;
    private LightSensor ls;

    Controller(TemperatureSensor ts, LightSensor ls)
    {
        this.ts=ts;
        this.ls=ls;
    }
    void control() throws InterruptedException
    {
        System.out.println(ts.readValue());
        Thread.sleep(1000);
        System.out.println(ls.readValue());
    }
}
