package man.sebastian.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    public int readValue()
    {
        Random generate=new Random();
        return generate.nextInt(100);
    }

}
