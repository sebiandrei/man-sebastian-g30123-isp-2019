package man.sebastian.lab5.ex4;

import man.sebastian.lab5.ex3.LightSensor;
import man.sebastian.lab5.ex3.TemperatureSensor;
public class SingletonController {

    private static volatile SingletonController instance = null;

    private SingletonController() {
    }

    public static SingletonController getInstance() {
        synchronized (SingletonController.class) {
            if (instance == null) {
                instance = new SingletonController();
            }
        }
        return instance;
    }
    private LightSensor lightSensor = new LightSensor();
    private TemperatureSensor temperatureSensor = new TemperatureSensor();
    private void control() {
        int i = 0;
        while (i < 20) {
            System.out.println("Temperature: "+temperatureSensor.readValue());
            System.out.println("Light: "+lightSensor.readValue());
            try {
                Thread.sleep(1000);
                System.out.println("---");
            } catch (InterruptedException e) {
                System.err.println(e);
            }
            i++;
        }
    }

    public static void main(String[] args) {
        SingletonController singletonController = new SingletonController();
        singletonController.control();
    }

}
