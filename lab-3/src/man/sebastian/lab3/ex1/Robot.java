package man.sebastian.lab3.ex1;

public class Robot {
    private int x;
    Robot() {
        x=1;
    }
    void change(int k){
        if(k>=1)
            this.x=this.x+k;
    }
    public String toString(){
        return "The current value is: " +this.x;
    }
}
