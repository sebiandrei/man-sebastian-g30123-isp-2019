package man.sebastian.lab3.ex5;

public class Flower{
    int petal;
    private static int numFlowers;
    private Flower(){
        numFlowers++;
        System.out.println("Flower has been created!");
    }

    public static int getNumFlowers() {
        return numFlowers;
    }

    public static void main(String[] args) {
        Flower[] garden;
        garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
    }
}