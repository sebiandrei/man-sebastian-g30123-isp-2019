package man.sebastian.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1=new MyPoint();
        MyPoint p2=new MyPoint(5,5);
        System.out.println(p1.distance(p2));
        System.out.println(p1.distance(3,3));
        System.out.println(p1.toString());
        System.out.println(p2.toString());
    }
}
