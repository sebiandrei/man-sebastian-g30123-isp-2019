package man.sebastian.lab3.ex4;

public class MyPoint {
    private int x, y;

    MyPoint() {
        this.x=0;
        this.y=0;
    }

    MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    private int getX() {
        return x;
    }

    private int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int y, int x) {
        this.y = y;
        this.x = x;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    double distance(int x, int y){
        return((Math.sqrt(Math.pow(this.getX()-x,2)+Math.pow(this.getY()-y,2))));
    }

    double distance(MyPoint another){
        return(Math.sqrt(Math.pow(this.getX()-another.getX(),2)+Math.pow(this.getY()-another.getY(),2)));
    }

}
