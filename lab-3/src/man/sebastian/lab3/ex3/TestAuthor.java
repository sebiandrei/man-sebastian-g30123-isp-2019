package man.sebastian.lab3.ex3;

public class TestAuthor {
    public static void main(String[] args) {
        Author a1=new Author("Mihai","mihai@eminescu.com",'m');
        Author a2=new Author("Andrei","andrei@gmail.com",'m');
        System.out.println(a1.toString());
        System.out.println(a2.toString());
        a2.setEmail("mihaitza@gmail.com");
        System.out.println(a2.getName()+" ("+a2.getGender()+") "+ "at " +a2.getEmail());

    }
}
