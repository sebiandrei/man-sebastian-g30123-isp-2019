package man.sebastian.lab3.ex3;

public class Author {

    private String name, email;
    private char gender;

    Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    String getName() {
        return name;
    }

    String getEmail() {
        return email;
    }

    char getGender() {
        return gender;
    }

    void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return name + " (" + gender + ") " + "at " + email;
    }
}