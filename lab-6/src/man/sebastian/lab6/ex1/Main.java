package man.sebastian.lab6.ex1;

public class Main {
    public static void main(String[] args)
    {
        BankAccount b=new BankAccount("owner1",300);
        BankAccount c=new BankAccount("owner2",500);
        b.withdraw(50);
        c.deposit(50);
        if (b.hashCode()==c.hashCode()){
            if(b.equals(c))
                System.out.println("Same");
            else
                System.out.println("Not the same");
        }
        else {
            System.out.println("Not the same(hash)");
        }
    }
}