package man.sebastian.lab7.ex1;

class CofeeMaker {
    private static int coffeeCount = 0;
    private int LIMIT = 5;

    Coffee makeCofee() throws MakeException {
        if (coffeeCount >= LIMIT) {
            throw (new MakeException("To much coffee made"));
        }
        System.out.println("Make a coffe");
        coffeeCount++;
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        return new Coffee(t, c);
    }
}

