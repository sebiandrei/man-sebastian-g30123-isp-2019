package man.sebastian.lab7.ex1;

class TemperatureException extends Exception {
    private int t;

    TemperatureException(int t, String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp() {
        return t;
    }
}
