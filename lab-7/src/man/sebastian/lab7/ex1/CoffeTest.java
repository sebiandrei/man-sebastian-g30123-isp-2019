package man.sebastian.lab7.ex1;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){

            try {
                Coffee c = mk.makeCofee();

                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
                }
                finally{
                    System.out.println("Throw the cofee cup.\n");
                }
            }
            catch(MakeException e)
            {
                System.out.println("Exception:" + e.getMessage());
            }
        }
    }
}