package man.sebastian.lab7.ex4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class CarMain {
    public static void main (String args[])
    {
        Car c = new Car("model1",500);
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Sebi\\man-sebastian-g30123-isp-2019\\lab-7\\src\\man\\sebastian\\lab7\\ex4\\car.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(c);
            out.close();
            fileOut.close();
            System.out.print("Serialized data is saved");
        } catch (IOException i) {
            i.printStackTrace();
        }
        Car c1;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Sebi\\man-sebastian-g30123-isp-2019\\lab-7\\src\\man\\sebastian\\lab7\\ex4\\car.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            c1 = (Car) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException ce) {
            System.out.println("Employee class not found");
            ce.printStackTrace();
            return;
        }

        System.out.println("\nDeserialized Employee...");
        System.out.println("Name: " + c1.getModel());
        System.out.println("Address: " + c1.getPrice());

    }
}
