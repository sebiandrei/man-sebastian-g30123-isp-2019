package man.sebastian.lab4.ex6;

public class Shape {
    private String color;
    private boolean filled;

    Shape() {
        color = "red";
        filled = true;
    }

    Shape(String color, boolean filled) {
        this();
        this.color = color;
        this.filled = filled;
    }

    String getColor() {
        return color;
    }

    void setColor(String color) {
        this.color = color;
    }

    boolean isFilled() {
        return filled;
    }

    void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}
