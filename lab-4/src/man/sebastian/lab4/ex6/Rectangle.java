package man.sebastian.lab4.ex6;

public class Rectangle extends Shape {
    private double width;
    private double lenght;

    Rectangle() {
        this.width = 1.0;
        this.lenght = 1.0;
    }

    Rectangle(double width, double lenght) {
        this.width = width;
        this.lenght = lenght;
    }

    Rectangle(String color, boolean filled, double width, double lenght) {
        super(color, filled);
        this.width = width;
        this.lenght = lenght;
    }

    double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    double getArea() {
        return this.lenght * this.width;
    }

    double getPerimeter() {
        return 2 * this.width + 2 * this.lenght;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", lenght=" + lenght +
                '}' + "which is a subclass of" + super.toString();
    }
}
