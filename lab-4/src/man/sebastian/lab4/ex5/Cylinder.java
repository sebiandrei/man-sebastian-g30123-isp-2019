package man.sebastian.lab4.ex5;

import man.sebastian.lab4.ex1.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
        //this.height = 1.0;
    }

    public Cylinder(double radius) {
        super(radius);
    }

    Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    double getHeight() {
        return height;
    }

    double getVolume() {
        return height * super.getArea();
    }
}