package man.sebastian.lab4.ex4;

import man.sebastian.lab4.ex2.Author;

import java.util.Arrays;

public class TestBook {
    public static void main(String[] args) {
        Author[] aut = new Author[3];
        aut[0] = new Author("Ion", "ceva@gmail.com", 'g');
        aut[1] = new Author("Vlad", "cevaa@gmail.com", 'g');
        aut[2] = new Author("Lavinia", "cevaaa@gmail.com", 'f');
        Book b = new Book("NewBook", aut, 200, 67);
        System.out.println(b.toString() + "\n");
        b.setPrice(2);
        b.setQtyInStock(500);
        b.printAuthors();
        System.out.println(Arrays.toString(b.getAuthor()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
    }
}
