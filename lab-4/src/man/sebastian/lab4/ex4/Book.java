package man.sebastian.lab4.ex4;

import man.sebastian.lab4.ex2.Author;

import java.util.Arrays;
public class Book {
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock;

    public Book() {
        this.qtyInStock = 0;
    }

    public Book(String name, Author[] author, double price) {
        super();
        this.name = name;
        this.author = author;
        this.price = price;
    }

    Book(String name, Author[] author, double price, int qtyInStock) {
        super();
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    String getName() {
        return name;
    }

    Author[] getAuthor() {
        return author;
    }

    double getPrice() {
        return price;
    }

    int getQtyInStock() {
        return qtyInStock;
    }

    void setPrice(double price) {
        this.price = price;
    }

    void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author=" + Arrays.toString(author) + ", price=" + price +
                ", qtyInStock=" + qtyInStock +
                '}';
    }

    void printAuthors() {
        System.out.println(Arrays.toString(author));
    }
}

