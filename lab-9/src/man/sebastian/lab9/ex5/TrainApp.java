package man.sebastian.lab9.ex5;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TrainApp extends JFrame {
    private JLabel inputTrain, inputStation, inputDestination, inputSegment;
    private JButton addTrain;
    private JTextField inTrain, inStation, inDestination, inSegment;
    private static JTextArea displayStation, info;
    private static int numberOfTrains = 0;
    private Train[] t = new Train[9];
    //build station Cluj-Napoca
    private Controler c1 = new Controler("Cluj-Napoca");

    private Segment s1 = new Segment(1);
    private Segment s2 = new Segment(2);
    private Segment s3 = new Segment(3);
    //build station Bucuresti
    private Controler c2 = new Controler("Bucuresti");

    private Segment s4 = new Segment(4);
    private Segment s5 = new Segment(5);
    private Segment s6 = new Segment(6);
    //build station Timisoara
    private Controler c3 = new Controler("Timisoara");

    private Segment s7 = new Segment(7);
    private Segment s8 = new Segment(8);
    private Segment s9 = new Segment(9);

    TrainApp() {
        numberOfTrains = 0;
        setTitle("Train control");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setSize(1200, 1000);
        setVisible(true);
        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        c3.addControlledSegment(s7);
        c3.addControlledSegment(s8);
        c3.addControlledSegment(s9);

        //connect the 3 controllers

        c1.setNeighbourController(c2);
        c2.setNeighbourController(c1);
        c1.setNeighbourController(c3);
        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);
        c3.setNeighbourController(c2);
    }

    private void init() {
        this.setLayout(null);
        int width = 150;
        int height = 40;

        inputTrain = new JLabel("Introduce Train name");
        inputTrain.setBounds(10, 20, width, height);

        inputStation = new JLabel("Current station");
        inputStation.setBounds(160, 20, width, height);

        inputDestination = new JLabel("Destination");
        inputDestination.setBounds(310, 20, width, height);

        inputSegment = new JLabel("On segment");
        inputSegment.setBounds(460, 20, width, height);

        info = new JTextArea("Bucuresti has 1,2,3 segements\nCluj-Napoca has 4,5,6 semgents\nTimisoara has 7,8,9 segmnets\n");
        info.setBounds(620, 20, 200, 60);
        info.setEditable(false);

        inTrain = new JTextField();
        inTrain.setBounds(10, 65, width, height);

        inStation = new JTextField();
        inStation.setBounds(160, 65, width, height);

        inDestination = new JTextField();
        inDestination.setBounds(310, 65, width, height);

        inSegment = new JTextField();
        inSegment.setBounds(460, 65, width, height);

        addTrain = new JButton("Add train");
        addTrain.setBounds(20, 110, width, height);
        addTrain.addActionListener(new AddButtonTreatment());

        displayStation = new JTextArea();
        displayStation.setBounds(20, 160, 800, 600);
        displayStation.setEditable(false);

        add(inDestination);
        add(inputDestination);
        add(inputSegment);
        add(inputStation);
        add(inputTrain);
        add(inTrain);
        add(inSegment);
        add(inStation);
        add(displayStation);
        add(addTrain);
        add(info);
    }

    class AddButtonTreatment implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (numberOfTrains < 9) {
                if (displayStation.getText() != null)
                    displayStation.setText(null);
                String stationName = TrainApp.this.inStation.getText();
                String trainName = TrainApp.this.inTrain.getText();
                String destinationName = TrainApp.this.inDestination.getText();
                int segmentId = Integer.parseInt(TrainApp.this.inSegment.getText());
                if (trainName != null) {
                    if (stationName != null) {
                        if (destinationName != null) {
                            if (TrainApp.this.inSegment.getText() != null) {
                                t[numberOfTrains] = new Train(TrainApp.this.inDestination.getText(), TrainApp.this.inTrain.getText());
                                if (stationName.equals("Bucuresti") && segmentId >= 1 && segmentId < 4) {
                                    c1.arriveTrain(t[numberOfTrains], segmentId);
                                    c1.controlStep();
                                }
                                if (stationName.equals("Cluj-Napoca") && segmentId >= 4 && segmentId < 7) {
                                    c2.arriveTrain(t[numberOfTrains], segmentId);
                                    c2.controlStep();
                                }
                                if (stationName.equals("Timisoara") && segmentId >= 7 && segmentId < 10) {
                                    c3.arriveTrain(t[numberOfTrains], segmentId);
                                    c3.controlStep();
                                }
                                inDestination.setText("");
                                inSegment.setText("");
                                inStation.setText("");
                                inTrain.setText("");
                            } else displayStation.append("Segment section isn't filled\n");
                        } else displayStation.append("Destination section isn't filled\n");
                    } else displayStation.append("Train name section isn't filled\n");
                } else displayStation.append("Station section isn't filled\n");
            } else displayStation.append("Number of trains exceed\n");
            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }

    public static final void systemOut(String s) {
        displayStation.append(s + "\n");
    }
}
