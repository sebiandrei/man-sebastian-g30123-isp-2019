package man.sebastian.lab8.ex4;

public class Gsm {
    String phoneNumber;
    Gsm(String phoneNumber){
        this.phoneNumber=phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Calling owner "+phoneNumber;
    }
}
