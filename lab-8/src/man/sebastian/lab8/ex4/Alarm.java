package man.sebastian.lab8.ex4;

public class Alarm {
    boolean alarm;

    Alarm(boolean alarm) {
        this.alarm = alarm;
        control();
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
        control();
    }

    void control() {
        if (alarm)
            System.out.println("Alarm started");
    }
}
