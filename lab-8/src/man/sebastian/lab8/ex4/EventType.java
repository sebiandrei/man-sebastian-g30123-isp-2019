package man.sebastian.lab8.ex4;

public enum EventType {
    TEMPERATURE, FIRE, NONE;
}
