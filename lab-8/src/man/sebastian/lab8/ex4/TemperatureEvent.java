package man.sebastian.lab8.ex4;

public class TemperatureEvent extends Event {
    private int value;

    TemperatureEvent(int value) {
        super(EventType.TEMPERATURE);
        this.value = value;
    }

    @Override
    public String toString() {
        return "TemperatureEvent{" + "value=" + value + '}';
    }
}
