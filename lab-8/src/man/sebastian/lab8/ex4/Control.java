package man.sebastian.lab8.ex4;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Control {
    private static Control single_instance = null;
    private Random r = new Random();
    private TemperatureSensor temperatureSensor;
    private List<FireSensor> fireSensors = new ArrayList<FireSensor>();
    private Alarm alarm;
    private Gsm gsm;

    private Control() {
        gsm = new Gsm("0755555555");
        alarm = new Alarm(false);
        temperatureSensor = new TemperatureSensor();
        addFireSensor();
    }

    /**
     * add new fire sensor in list
     */
    private void addFireSensor() {
        FireSensor fireSensor = new FireSensor();
        fireSensors.add(fireSensor);
    }

    /**
     * control if there is any event happening
     * @return the type of the event
     */
    public Event control() {
        for (FireSensor firesensor : fireSensors) {
            if (firesensor.getSmoke()) {
                alarm.setAlarm(true);
                gsm.toString();
                System.out.println("Fire Started");
                System.out.println("Starting the Alarm");
                System.out.println("Calling the owner");
                return new FireEvent(true);
            }
        }
        if (temperatureSensor.getTemperature() > TemperatureOptimizer.REFERENCE_TEMPERATURE + TemperatureOptimizer.VARIATION) {
            System.out.println("Start cooling");
            int x = r.nextInt(3) + 14;
            temperatureSensor.setTemperature(x);
            System.out.println("The house is cooling");
            return new TemperatureEvent(x);
        }
        if (temperatureSensor.getTemperature() < TemperatureOptimizer.REFERENCE_TEMPERATURE - TemperatureOptimizer.VARIATION) {
            System.out.println("Start heating");
            int x = r.nextInt(7) + 23;
            temperatureSensor.setTemperature(x);
            System.out.println("The house is heating");
            return new TemperatureEvent(x);
        }
        return new NoEvent();
    }

    public static Control getInstance() {
        if (single_instance == null)
            single_instance = new Control();

        return single_instance;
    }
}
