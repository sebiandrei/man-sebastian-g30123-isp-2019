package man.sebastian.lab8.ex4;

public class HomeAutomation {
    public static void main(String[] args) {
        //test using an annonimous inner class
        Home h = new Home() {
            protected void setValueInEnvironment(Event event) {
                System.out.println("New event in environment " + event);
            }
        };
        h.simulate();
    }
}
