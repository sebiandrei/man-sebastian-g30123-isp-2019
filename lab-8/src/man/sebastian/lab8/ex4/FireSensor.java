package man.sebastian.lab8.ex4;

import java.util.Random;

public class FireSensor {
    private boolean smoke;
    private Random r = new Random();
    public FireSensor(){
        smoke=r.nextBoolean();
    }

    public boolean getSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        if(smoke)
            return "There is smoke";
        else return "There is no smoke";
    }
}
