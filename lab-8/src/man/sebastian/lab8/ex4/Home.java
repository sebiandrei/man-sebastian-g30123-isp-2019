package man.sebastian.lab8.ex4;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

abstract class Home {
    private Control control;
    private final int SIMULATION_STEPS = 20;

    /**
     * @param event setting a specific event in environment
     */
    protected abstract void setValueInEnvironment(Event event);

    /**
     * simulate house events for 20 execution step, each one during about 300 ms each
     * and verify if there is any event happening and log intro the file
     */
    public void simulate() {
        int k = 0;
        control = Control.getInstance();
        try {
            PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("C:\\Users\\Denis\\chisalita-denis-g30125-isp-2018\\lab8\\src\\main\\java\\chisalita\\denis\\lab8\\ex4\\system_logs.txt")));
            System.setOut(out);
            while (k < SIMULATION_STEPS) {
                Event event = control.control();
                setValueInEnvironment(event);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                k++;
            }
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
